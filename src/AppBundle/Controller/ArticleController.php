<?php

// /src/AppBundle/Controller/ArticleController.php

namespace AppBundle\Controller;

use AppBundle\Entity\Article;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\RouteRedirectView;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Routing\ClassResourceInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use FOS\RestBundle\Controller\Annotations\RouteResource;

/**
 * Class ArticleController
 * @package AppBundle\Controller
 *
 */
class ArticleController extends FOSRestController implements ClassResourceInterface
{

    /**
     * Gets an individual Artilce
     *
     * @param int $id
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @ApiDoc(
     *     output="AppBundle\Entity\Article",
     *     statusCodes={
     *         200 = "Returned when successful",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function getAction($id)
    {
        $artilce = $this->getDoctrine()->getRepository('AppBundle:Article')->find($id);

        if ($artilce == null) {
            return new View(null, Response::HTTP_NOT_FOUND);
        }

        return $artilce;
    }

    /**
     * Gets a collection of Artilce
     *
     * @return array
     *
     * @ApiDoc(
     *     output="AppBundle\Entity\Article",
     *     statusCodes={
     *         200 = "Returned when successful",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function cgetAction()
    {
        return $this->getDoctrine()->getRepository('AppBundle:Article')->findAll();
    }

    /**
     * @param Request $request
     * @return View|\Symfony\Component\Form\Form
     *
     * @ApiDoc(
     *     input="AppBundle\Form\Type\ArticleType",
     *     output="AppBundle\Entity\ArticleType",
     *     statusCodes={
     *         201 = "Returned when a new Article has been successful created",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function postAction(Request $request)
    {
        $form = $this->createForm('AppBundle\Form\ArticleType', null, [
            'csrf_protection' => false,
        ]);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $form;
        }
        /**
         * @var $article Article
         */
        $article = $form->getData();

        $em = $this->getDoctrine()->getManager();
        $em->persist($article);
        $em->flush();

        $routeOptions = [
            'id'      => $article->getId(),
            '_format' => $request->get('_format'),
        ];

        return $this->routeRedirectView('get_article', $routeOptions, Response::HTTP_CREATED);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return View|\Symfony\Component\Form\Form
     *
     * @ApiDoc(
     *     input="AppBundle\Form\Type\ArticleType",
     *     output="AppBundle\Entity\Article",
     *     statusCodes={
     *         204 = "Returned when an existing Article has been successful updated",
     *         400 = "Return when errors",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function putAction(Request $request, $id)
    {
        /**
         * @var $article Article
         */
        $article = $this->getDoctrine()->getRepository('AppBundle:Article')->find($id);
        if ($article === null) {
            return new View(null, Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm('AppBundle\Form\ArticleType', $article, [
            'csrf_protection' => false,
        ]);

        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return $form;
        }

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $routeOptions = [
            'id'      => $article->getId(),
            '_format' => $request->get('_format'),
        ];

        return $this->routeRedirectView('get_article', $routeOptions, Response::HTTP_NO_CONTENT);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return View|\Symfony\Component\Form\Form
     *
     * @ApiDoc(
     *     input="AppBundle\Form\Type\ArticleType",
     *     output="AppBundle\Entity\Article",
     *     statusCodes={
     *         204 = "Returned when an existing Article has been successful updated",
     *         400 = "Return when errors",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function patchAction(Request $request, $id)
    {
        /**
         * @var $article Article
         */
        $article = $this->getDoctrine()->getRepository('AppBundle:Article')->find($id);

        if ($article === null) {
            return new View(null, Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm('AppBundle\Form\ArticleType', $article, [
            'csrf_protection' => false,
        ]);

        $form->submit($request->request->all(), false);

        if (!$form->isValid()) {
            return $form;
        }

        $em = $this->getDoctrine()->getManager();
        $em->flush();

        $routeOptions = [
            'id'      => $article->getId(),
            '_format' => $request->get('_format'),
        ];

        return $this->routeRedirectView('get_article', $routeOptions, Response::HTTP_NO_CONTENT);
    }

    /**
     *
     * @param type $id
     * @return View
     *
     * @ApiDoc(
     *     statusCodes={
     *         204 = "Returned when an existing Article has been successful deleted",
     *         404 = "Return when not found"
     *     }
     * )
     */
    public function deleteAction($id)
    {
        /**
         * @var $article Article
         */
        $article = $this->getDoctrine()->getRepository('AppBundle:Article')->find($id);

        if ($article === null) {
            return new View(null, Response::HTTP_NOT_FOUND);
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($article);
        $em->flush();

        return new View(null, Response::HTTP_NO_CONTENT);
    }
}
